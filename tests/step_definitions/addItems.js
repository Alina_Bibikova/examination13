const {I} = inject();
// Add in your custom step files

When('я нахожусь на странице Все заведения', () => {
    I.amOnPage('items/');
});

When('я нажимаю кнопку {string}', fieldName => {
    I.click({xpath:`//button[@id='${fieldName}']`});
});

When('я ввожу {string} в поле {string}', (text, fieldName) => {
    I.fillField({xpath: `//input[@id='${fieldName}']`}, text)
});


