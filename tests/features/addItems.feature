# language: ru

Функция: Добавить новый магазин

  @addItems
  Сценарий: Добавить новый магазин
    Допустим я нахожусь на странице Войти
    Если я ввожу "user" в поле "username"
    И я ввожу "123" в поле "password"
    И нажимаю на кнопку "Войти"
    И я вижу текст "Вы вошли успешно"
    И я нахожусь на странице Все заведения
    И я нажимаю на кнопку "newItem"
    И я ввожу "test" в поле "title"
    И я ввожу "test" в поле "description"
    И я нажимаю на кнопку "question"
    И я нажимаю кнопку для отправки "addNewItems"
