import React from 'react';
import {Nav, NavItem, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

const UserMenu = ({user, logout}) => {
    return (
        <Nav>
            <NavItem>
                <NavLink tag={RouterNavLink} to={`/users/${user._id}`}>Привет, {user.displayName}</NavLink>
            </NavItem>
            <NavItem>
                <span className="nav-link" onClick={logout}>Выйти</span>
            </NavItem>
        </Nav>
    );
};

export default UserMenu;
