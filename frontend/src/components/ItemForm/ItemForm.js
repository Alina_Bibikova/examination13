import React, {Component} from 'react';
import {Button, Form} from "reactstrap";
import FormElement from "../UI/Form/FormElement";
import Label from "reactstrap/es/Label";
import FormGroup from "reactstrap/es/FormGroup";
import Col from "reactstrap/es/Col";
import Input from "reactstrap/es/Input";

class ItemForm extends Component {
    state = {
        title: '',
        image: null,
        description: '',
        question: ''

    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    getFieldHasError = fieldName => {
        return (
            this.props.error &&
            this.props.error.errors &&
            this.props.error.errors[fieldName] &&
            this.props.error.errors[fieldName].message
        );
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            if (this.state[key]) {
                formData.append(key, this.state[key]);
            }
        });

        this.props.add(formData, this.props.user._id);
    };

    render() {
        return (
            <div className="box p-3">
                <Form onSubmit={this.submitFormHandler}>
                    <FormElement
                        propertyName="title"
                        title="Название:"
                        id="title"
                        type="text"
                        value={this.state.title}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldHasError('title')}
                    />

                    <FormGroup row>
                        <Col sm={10}>
                            <Input
                                title="Описание:"
                                type="textarea" required
                                name="description" id="description"
                                placeholder="Введите описание"
                                value={this.state.description}
                                onChange={this.inputChangeHandler}
                                error={this.getFieldHasError('description')}
                            />
                        </Col>
                    </FormGroup>

                    <FormElement
                        propertyName="image"
                        id='image'
                        type="file"
                        onChange={this.fileChangeHandler}
                        error={this.getFieldHasError('image')}
                    />

                    <FormGroup row>
                        <Col sm={10}>
                            <Input
                                style={{marginLeft: '10px'}}
                                title={'Я согласен'}
                                type="checkbox" required
                                name="question" id="question"
                                value={this.state.question}
                                onChange={this.inputChangeHandler}
                                error={this.getFieldHasError('question')}
                            />
                        </Col>
                        <Label check for="2"
                               style={{marginLeft: '50px'}}
                        >
                            Вы согласны с условиями сайта?
                        </Label>
                    </FormGroup>

                    <Button
                        style={{marginRight: '10px'}}
                        type="submit"
                        color="info"
                        id="addNewItems"
                    >
                        Сохранить
                    </Button>

                </Form>
            </div>
        );
    }
}

export default ItemForm;