import React from 'react';
import {Button, Card, CardBody, CardFooter, CardImg, CardText, CardTitle, Col} from "reactstrap";
import {apiURL} from "../../constants";
import {NavLink as RouterNavLink} from "react-router-dom";
import Rating from "react-rating";


const Item = props => {
    return (
        <Col xs="12" sm="6" md="4">
            <Card className="mb-3">
                {props.image
                    ?
                    <CardImg src={`${apiURL}/uploads/${props.image}`} alt={props.title}/>

                    : null
                }

                {props.title  ?
                    <CardBody>
                        <CardTitle>
                            {props.description}
                        </CardTitle>
                        <CardText
                            tag={RouterNavLink}
                            to={`/items/${props.id}`}
                        >
                            {props.title}
                        </CardText>
                    </CardBody>
                    : null }
                {props.user && props.user.role === 'admin' ? <CardFooter>
                     <Button style={{marginRight: '10px'}} color='danger' onClick={props.deleteItems}>Удалить</Button>

                </CardFooter> : null}

                {props.items && props.item.rating ?
                    <CardFooter>
                        <p><Rating readonly
                                   stop={10}
                                   placeholderRating={this.averageRating(props.items.rating)}/></p>
                        <p>Количество оценок {props.items.rating.length}</p>
                        <p>Средняя оценка: {this.averageRating(props.items.rating)}</p>
                    </CardFooter>: null}

            </Card>
        </Col>
    );
};

export default Item;