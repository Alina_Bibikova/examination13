import axios from 'axios';
import config from "./constants";

const instance = axios.create({
    baseURL: config.apiURL
});

export default instance;