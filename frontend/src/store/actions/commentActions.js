import axios from '../../axios-api';
import {NotificationManager} from "react-notifications";


export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';

export const fetchCommentsSuccess = comments => ({type: FETCH_COMMENTS_SUCCESS, comments});

export const fetchComments = id => {
    return dispatch => {
        return axios.get(`/comments/${id}`).then(
            response => dispatch(fetchCommentsSuccess(response.data))
        )
    }
};

export const createComment = (commentData, id) => {
    return dispatch => {
        return axios.post('/comments', commentData).then(
            response => {
                dispatch(fetchComments(id));
                NotificationManager.success(response.data.message);
            }
        );
    };
};
export const deleteComment = (id, itemId) => {
    return (dispatch) => {
        return axios.delete(`/comments/${id}`).then(
            response => {
                dispatch(fetchComments(itemId));
                NotificationManager.warning(response.data.message);
            }
        )
    }
};