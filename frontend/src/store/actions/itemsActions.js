import axios from '../../axios-api';
import {push} from 'connected-react-router';
import {NotificationManager} from 'react-notifications';
import {
    ADD_DATA_FAILURE,
    ADD_DATA_REQUEST,
    ADD_DATA_SUCCESS, DELETE_DATA_FAILURE, DELETE_DATA_REQUEST, DELETE_DATA_SUCCESS,
    FETCH_DATA_FAILURE,
    FETCH_DATA_REQUEST, FETCH_ITEMS_SUCCESS, GET_AUTHOR_SUCCESS, RESET_AUTHOR, FETCH_ITEMID_SUCCESS
} from "./actionTypes";

const fetchDataRequest = () => ({type: FETCH_DATA_REQUEST});
const fetchDataFailure = error => ({type: FETCH_DATA_FAILURE, error});

const addDataRequest = () => ({type: ADD_DATA_REQUEST});
const addDataFailure = error => ({type: ADD_DATA_FAILURE, error});
const addDataSuccess = () => ({type: ADD_DATA_SUCCESS});

const deleteDataRequest = () => ({type: DELETE_DATA_REQUEST});
const deleteDataFailure = error => ({type: DELETE_DATA_FAILURE, error});
const deleteDataSuccess = () => ({type: DELETE_DATA_SUCCESS});

const fetchItemsSuccess = items => ({type: FETCH_ITEMS_SUCCESS, items});
const getAuthorSuccess = author => ({type: GET_AUTHOR_SUCCESS, author});
const resetAuthor = () => ({type: RESET_AUTHOR});

const fetchItemSuccess = itemId => ({type: FETCH_ITEMID_SUCCESS, itemId});

export const fetchItems = user => {
    return async dispatch => {
        let url = '/items';

        if (user) url += `?user=${user}`;

        dispatch(fetchDataRequest());

        try {
            const response = await axios.get(url);
            dispatch(fetchItemsSuccess(response.data));
        } catch (e) {
            dispatch(fetchDataFailure(e));
        }
    }
};

export const fetchItem = itemId => {
    return async dispatch => {
        dispatch(fetchDataRequest());

        try {
            const response = await axios.get(`/items/${itemId}`);
            dispatch(fetchItemSuccess(response.data));
        } catch (e) {
            NotificationManager.error(e.response.data.message);
            dispatch(fetchDataFailure(e));
        }
    }
};

export const getAuthor = id => {
    return async dispatch => {
        if (id) {
            try {
                const response = await axios.get(`/users/author/${id}`);
                dispatch(getAuthorSuccess(response.data));
            } catch (e) {
                NotificationManager.error(e.response.data.message);
            }
        } else {
            dispatch(resetAuthor());
        }
    }
};

export const add = (itemData) => {
    return async dispatch => {
        dispatch(addDataRequest());

        try {
            const response = await axios.post('/items', itemData);

            dispatch(addDataSuccess());
            NotificationManager.success(response.data.message);
            dispatch(push('/'));
        } catch (e) {
            dispatch(addDataFailure(e.response.data));
        }
    }
};

export const deleteItems = (itemsId) => {
    return async dispatch => {

        dispatch(deleteDataRequest());

        try {
            await axios.delete(`/items/${itemsId}`);
            NotificationManager.success('Удален!');
            await dispatch(deleteDataSuccess());
        } catch (e) {
            NotificationManager.error(e.response.data.message);
            dispatch(deleteDataFailure(e));
        }
    }
};

export const sendRating = (id, data) => {
    return dispatch => {
        return axios.put(`/items/${id}`, data).then(
            response => {
                dispatch(addDataSuccess());
                NotificationManager.success(response.data.message);
                dispatch(fetchItemSuccess(id));
                dispatch(push('/items/' + id));
            },
            error => {
                NotificationManager.warning(error.response.data);
            }
        );
    };
};