import {
    ADD_DATA_FAILURE,
    ADD_DATA_REQUEST, ADD_DATA_SUCCESS,
    FETCH_DATA_FAILURE,
    FETCH_DATA_REQUEST,
    FETCH_ITEMS_SUCCESS, GET_AUTHOR_SUCCESS, RESET_AUTHOR, FETCH_ITEMID_SUCCESS
} from "../actions/actionTypes";

const initialState = {
    items: [],
    author: '',
    item: {},
    loading: true,
    error: null
};

const itemsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_DATA_REQUEST:
            return {...state, loading: true};

        case FETCH_DATA_FAILURE:
            return {...state, loading: false, error: action.error};

        case FETCH_ITEMS_SUCCESS:
            return {...state, loading: false, items: action.items};

        case GET_AUTHOR_SUCCESS:
            return {...state, author: action.author};

        case FETCH_ITEMID_SUCCESS:
            return {...state, item: action.itemId};

        case RESET_AUTHOR:
            return {...state, author: ''};

        case ADD_DATA_REQUEST:
            return {...state, loading: true};

        case ADD_DATA_FAILURE:
            return {...state, loading: false, error: action.error};

        case ADD_DATA_SUCCESS:
            return {...state, error: null};

        default:
            return state
    }
};

export default itemsReducer;