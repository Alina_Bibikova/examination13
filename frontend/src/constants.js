export const apiURL = 'http://localhost:8000';

const config = {
    apiURL: 'http://localhost:8000',
    wsURL: 'ws://localhost:8000'
};

switch (process.env.REACT_APP_ENV) {
    case 'test':
        config.apiURL = 'http://localhost:8010';
        config.wsURL = 'ws://localhost:8010';
        break;
    default:
}

export default config;