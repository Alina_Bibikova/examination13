import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Alert, Button, Col, Form, FormGroup} from "reactstrap";

import {registerUser} from "../../store/actions/usersActions";
import FormElement from "../../components/UI/Form/FormElement";

class Register extends Component {
    state = {
        username: '',
        password: '',
        displayName: '',
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    submitFromHandler = event => {
        event.preventDefault();

        this.props.registerUser({...this.state});
    };

    getFieldHasError = fieldName => {
        return (
            this.props.error &&
            this.props.error.errors &&
            this.props.error.errors[fieldName] &&
            this.props.error.errors[fieldName].message
        );
    };

    render() {
        return (
            <Fragment>
                <h2 className="my-3 text-center">Зарегистрировать нового пользователя!</h2>
                <div className="box user-form p-4">
                    {this.props.error && this.props.error.global && (
                        <Alert color="danger">
                            {this.props.error.global}
                        </Alert>
                    )}
                    <Form onSubmit={this.submitFromHandler}>

                        <FormElement
                            propertyName="username"
                            title="Логин"
                            type="text"
                            value={this.state.username}
                            onChange={this.inputChangeHandler}
                            error={this.getFieldHasError('username')}
                            placeholder="Введите логин"
                            autoComplete="new-username"
                        />

                        <FormElement
                            propertyName="displayName"
                            title="Ваше полное имя"
                            type="text"
                            value={this.state.displayName}
                            onChange={this.inputChangeHandler}
                            error={this.getFieldHasError('displayName')}
                            placeholder="Введите полное имя"
                        />

                        <FormElement
                            propertyName="password"
                            title="Пароль"
                            type="password"
                            value={this.state.password}
                            onChange={this.inputChangeHandler}
                            error={this.getFieldHasError('password')}
                            placeholder="Введите пароль"
                            autoComplete="new-password"
                        />

                        <FormGroup className="mt-4">
                            <Col sm={{offset: 2, size: 10}}>
                                <Button type="submit" color="info" className="w-120">Регистрация</Button>
                            </Col>
                        </FormGroup>
                    </Form>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
    registerUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);