import React, {Component} from 'react';
import {connect} from "react-redux";

import {add} from "../../store/actions/itemsActions";
import ItemForm from "../../components/ItemForm/ItemForm";

class NewItem extends Component {
    render() {
        return (
            <div>
                <h3 className="mb-3">Добавить заведение</h3>
                <ItemForm
                    add={this.props.add}
                    user={this.props.user}
                    error={this.props.error}
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    error: state.items.error,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    add: (itemData, userId) => dispatch(add(itemData, userId))
});

export default connect(mapStateToProps, mapDispatchToProps)(NewItem);