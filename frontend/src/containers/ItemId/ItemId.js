import React, {Component} from 'react';
import {connect} from "react-redux";
import {deleteItems, fetchItem, sendRating} from "../../store/actions/itemsActions";
import Loader from "../../components/UI/Loader/Loader";
import {apiURL} from "../../constants";
import {Button, Card, CardFooter, Col} from "reactstrap";
import CardImg from "reactstrap/es/CardImg";
import {createComment, deleteComment, fetchComments} from "../../store/actions/commentActions";
import Form from "reactstrap/es/Form";
import Input from "reactstrap/es/Input";
import Alert from "reactstrap/es/Alert";
import CardBody from "reactstrap/es/CardBody";
import Rating from "react-rating";
import CardText from "reactstrap/es/CardText";
import Modal from "reactstrap/es/Modal";
import ModalHeader from "reactstrap/es/ModalHeader";
import ModalBody from "reactstrap/es/ModalBody";
import FormElement from "../../components/UI/Form/FormElement";
import {fetchPhotos, getItem} from "../../store/actions/photosActions";

class ItemId extends Component {
    constructor(props) {
        super(props);
    this.state = {
        description: '',
        item: this.props.match.params.id,
        date: new Date(),
        image: null
    };
    this.toggle = this.toggle.bind(this);
}

    componentDidMount()  {
        const id = this.props.match.params.id;
        this.props.fetchItem(id);
        this.props.fetchComments(id);
    }

    setAnonymous = () => {
        this.setState({anonymous: !this.state.anonymous});
    };
    toggle = () => {
        this.setState(prevState => ({
            modal: !prevState.modal,
        }));
    };

    inputChangeHandler = event => {
        this.setState({[event.target.name]: event.target.value})
    };

    submitChangeHandler = event => {
        event.preventDefault();
        this.props.createComment({...this.state}, this.props.match.params.id)

        this.setState({description: '', review: '', rating: 0});
        let rating = {
            items: this.props.item._id,
            rating: this.state.rating,
            review: this.state.review,
            anonymous: this.state.anonymous,
            displayName: this.state.displayName

        };
        this.props.sendRating(this.props.item.item._id, rating);
        this.toggle();
    };

    averageRating = data => {
        const ratingsNum = data.length;
        const sum = data.reduce((acc, currentVal) => acc += currentVal.rating || 0, 0);
        return (sum === 0 && ratingsNum === 0) ? 0 : Math.round(sum / ratingsNum);

    };

    makeRating = value => {
        this.setState({rating: value});
    };

    render() {
        const item = this.props.item;
        const userId = this.props.user && this.props.user._id && this.props.user.displayName;

        return (
            <Col xs="12" sm="6" md="12">

                {this.props.loading && <Loader/>}

                <Card style={{padding: '10px'}} >
                    <h3>Заведение</h3>
                    {userId && item ?
                        <p className="mb-3">Вы зашли как: {userId}</p>
                        : null}

                    <p>
                        <strong>
                            Название: {item.title}
                        </strong>
                    </p>
                    <p>
                        Описание: {item.description}
                    </p>
                    {item.image && (
                        <CardImg
                            top width="100%" src={`${apiURL}/uploads/${item.image}`} className="item-img" alt={item.title}/>
                    )}

                    <CardText>
                        <Button className="my-3" color = "info" onClick={() => this.toggle()}>Оценить  заведение</Button>
                    </CardText>

                    {this.props.item && this.props.item.rating ?
                        <CardText>
                            <p><Rating readonly
                                       stop={10}
                                       placeholderRating={this.averageRating(this.props.item.rating)}/></p>
                            <p>Количество оценок {this.props.item.rating.length}</p>
                        <p>Средняя оценка: {this.averageRating(this.props.item.rating)}</p>
                    </CardText>: null}


                    {userId && userId.role === 'admin' ? <CardFooter>
                        <Button style={{marginRight: '10px'}} color='danger' onClick={(itemId) => this.props.deleteItems(itemId)}>Удалить</Button>

                    </CardFooter> : null}
                </Card>

                {this.props.comments || this.props.comments.length <= 0 ? null :
                     <div>
                        <h2>Комментарии:</h2>
                        {this.props.comments.map((item) => {
                            return (
                                <Card key={item._id}
                                      style={{margin: '20px'}}>
                                    <CardFooter>Автор: {item.user}</CardFooter>
                                    <CardBody>{item.description}</CardBody>
                                    <div style={{padding: '20px'}} className="delete-button ml-auto">
                                        <Button
                                            id="deleteComment"
                                            color="danger"
                                            onClick={() => this.props.deleteComment(item._id, item.item._id)}
                                        >
                                            Удалить комментарий
                                        </Button>
                                    </div>
                                </Card>
                            )
                        })}
                    </div>
                }

                {!this.props.user ? <Alert color='danger'>Зарегестируйтесь чтобы оставить комментарий</Alert> : <Form>
                    <h5>Хотите добавить комметарий?</h5>
                    <Input type='text'
                           id="comment"
                           name='description'
                           value={this.state.description}
                           onChange={this.inputChangeHandler}/>
                    <Button id="addComment" color="primary" style={{margin: '20px'}}
                            onClick={this.submitChangeHandler}>Добавить</Button>
                </Form>}

                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Оцените заведение</ModalHeader>
                    <ModalBody>
                        <Form>
                            <h3>Оцените заведение от 1 до 10</h3>
                            <Rating
                                stop={10} onClick={value => this.makeRating(value)}
                                placeholderRating={this.state.rating}
                            />
                            <h3>Оставьте свой отзыв</h3>
                            <FormElement
                                propertyName="review"
                                type="text"
                                value={this.state.review}
                                onChange={this.inputChangeHandler}
                            />
                            <FormElement className="check-input"
                                         propertyName="anonymous"
                                         type="checkbox"
                                         title="Оставить отзыв анонимно"
                                         value={this.state.anonymous}
                                         onChange={this.setAnonymous}
                            />
                            <Button color='success' onClick={this.submitChangeHandler}>Отправить
                                отзыв и оценку</Button>
                        </Form>

                    </ModalBody>
                </Modal>
            </Col>
        );
    }
}

const mapStateToProps = state => ({
    comments: state.comments.comments,
    item: state.items.item,
    user: state.users.user,
    error: state.items.error,
    loading: state.items.loading
});

const mapDispatchToProps = dispatch => ({
    fetchPhotos: user => dispatch(fetchPhotos(user)),
    getItem: id => dispatch(getItem(id)),
    fetchItem: itemId => dispatch(fetchItem(itemId)),
    deleteItems: itemId => dispatch(deleteItems(itemId)),
    fetchComments: id => dispatch(fetchComments(id)),
    createComment: (data, id) => dispatch(createComment(data, id)),
    deleteComment: (id, itemId) => dispatch(deleteComment(id, itemId)),
    sendRating: (id, data) => dispatch(sendRating(id, data))
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemId);