import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Alert, Button, Col, Form, FormGroup} from "reactstrap";
import {loginUser} from "../../store/actions/usersActions";
import FormElement from "../../components/UI/Form/FormElement";

class Login extends Component {
    state = {
        username: '',
        password: '',
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    submitFormHandler = event => {
        event.preventDefault();
        this.props.loginUser({...this.state})
    };

    render() {
        return (
            <Fragment>
                <h2 className="my-3 text-center">Войти</h2>
                <div className="box user-form p-4">
                    {this.props.error && (
                        <Alert color="danger">
                            {this.props.error.error || this.props.error.global}
                        </Alert>
                    )}
                    <Form onSubmit={this.submitFormHandler}>

                        <FormElement
                            propertyName="username"
                            title="Логин"
                            type="text"
                            value={this.state.username}
                            onChange={this.inputChangeHandler}
                            placeholder="Введите логин"
                            autoComplete="current-username"
                        />

                        <FormElement
                            propertyName="password"
                            title="Пароль"
                            type="password"
                            value={this.state.password}
                            onChange={this.inputChangeHandler}
                            placeholder="Введите пароль"
                            autoComplete="current-password"
                        />

                        <FormGroup row>
                            <Col sm={{offset: 2, size: 10}}>
                                <Button type="submit" color="info">Войти</Button>
                            </Col>
                        </FormGroup>
                    </Form>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.loginError
});

const mapDispatchToProps = dispatch => ({
    loginUser: userData => dispatch(loginUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);