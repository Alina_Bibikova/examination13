import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {NavLink as RouterNavLink} from 'react-router-dom';
import {Button, Row} from "reactstrap";

import {deleteItems, fetchItems, getAuthor} from "../../store/actions/itemsActions";
import Loader from "../../components/UI/Loader/Loader";
import Item from "../../components/Item/Item";

class Items extends Component {
    componentDidMount() {
        this.props.fetchItems(this.props.match.params.user);
        this.props.getAuthor(this.props.match.params.user);
    }

    componentDidUpdate(prevProps) {
        if (this.props.match.params.user !== prevProps.match.params.user) {
            this.props.fetchItems(this.props.match.params.user);
            this.props.getAuthor(this.props.match.params.user);
        }
    };

    render() {

        return (
            <Fragment>
                <h3 className="mb-3">Все заведения</h3>

                {this.props.loading && <Loader/>}

                <h2>
                    {this.props.user &&
                        <Button tag={RouterNavLink} to="/item/new"
                                color="info"
                                id="newItem"
                                className="float-right"
                        >
                            Добавить новое заведение
                        </Button>
                    }
                </h2>

                <Row>
                    {this.props.items.map(items => {
                        return (
                            <Item
                                id={items._id}
                                key={items._id}
                                title={items.title}
                                image={items.image}
                                author={items.user}
                                user={this.props.user}
                                description={items.description}
                                rating={items.rating}
                                deleteItems={() => this.props.deleteItems(items._id, this.props.user._id)}
                            />
                        )
                    })}
                </Row>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    items: state.items.items,
    error: state.items.error,
    loading: state.items.loading
});

const mapDispatchToProps = dispatch => ({
    fetchItems: user => dispatch(fetchItems(user)),
    getAuthor: id => dispatch(getAuthor(id)),
    deleteItems: (itemsId, userId) => dispatch(deleteItems(itemsId, userId))
});

export default connect(mapStateToProps, mapDispatchToProps)(Items);