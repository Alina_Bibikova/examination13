const express = require('express');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const config = require('../config');

const auth = require('../middleware/auth');
const Photo = require('../models/Photo');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
    const criteria = {};

    if (req.query.items) {
        criteria.items = req.query.items;
    }

    try {
        const photos = await Photo.find(criteria);

        return res.send(photos);
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.post('/', [auth, upload.single('image')], async (req, res) => {
    const photoData = {
        items: req.items._id
    };

    if (req.file) {
        photoData.image = req.file.filename;
    }

    try {
        const photo = new Photo(photoData);

        await photo.save();
        return res.send({message: 'Добавлено!', photo});
    } catch (e) {
        return res.status(400).send(e);
    }
});

module.exports = router;