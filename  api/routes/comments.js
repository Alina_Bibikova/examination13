const express = require('express');

const auth = require('../middleware/auth');
const Comment = require('../models/Comment');

const router = express.Router();

router.post('/', auth, (req, res) => {
    const commentData = {...req.body, user: req.user._id};

    const comment = new Comment(commentData);

    comment.save()
        .then(result => res.send({result, message: 'Ваш комментарий добавлен'}))
        .catch(error => res.status(400).send(error));
});

router.get('/:id', (req, res) => {
    Comment.find({item: req.params.id}).populate('item').populate('user')
        .then(comment => {
            if (comment) res.send(comment);
            else res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500));
});

router.delete('/:id', [auth], (req, res) => {
    Comment.findByIdAndDelete({_id: req.params.id})
        .then(result => res.send({result, message: 'Ваш комментарий удален'}))
        .catch(error => res.status(403).send(error))
});

module.exports = router;