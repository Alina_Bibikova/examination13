const express = require('express');
const axios = require('axios');
const nanoid = require('nanoid');

const config = require('../config');
const User = require('../models/User');

const router = express.Router();

router.get('/author/:id', async (req, res) => {
    try {
        const author = await User.findById(req.params.id, 'displayName');
        return res.send(author);
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.post('/', async (req, res) => {
    try {
        const user = new User({
            username: req.body.username,
            displayName: req.body.displayName,
            password: req.body.password,
        });

        user.generateToken();
        await user.save();

        return res.send({message: 'Успешная регистрация!', user});
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.post('/sessions', async (req, res) => {
    const user = await User.findOne({username: req.body.username});
    if (!user) return res.status(400).send({message: 'Неверный логин или пароль!'});

    const isMatch = await user.checkPassword(req.body.password);
    if (!isMatch) return res.status(400).send({message: 'Неверный логин или пароль!'});

    user.generateToken();
    await user.save();

    return res.send({message: 'Вы вошли успешно!', user});
});

router.delete('/sessions', async (req, res) => {
    const success = {message: 'Вы вышли успешно!'};

    const token = req.get('Authorization');
    if (!token) return res.send(success);

    const user = await User.findOne({token});
    if (!user) return res.send(success);

    user.generateToken();
    await user.save();

    return res.send(success);
});

module.exports = router;