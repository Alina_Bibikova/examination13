const express = require('express');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const config = require('../config');
const auth = require('../middleware/auth');
const Item = require('../models/Item');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.get('/items/:id', async (req, res) => {
    try {
        const item = await Item.findById(req.params.id);
        return res.send(item);
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.get('/', async (req, res) => {
    const criteria = {};

    if (req.query.user) {
        criteria.user = req.query.user;
    }

    try {
        const items = await Item.find(criteria).populate('user', 'displayName');

        return res.send(items);
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.post('/', [auth, upload.single('image')], async (req, res) => {
    const itemData = {
        title: req.body.title,
        user: req.user._id,
        description: req.body.description,
        question: req.body.question
    };

    if (req.file) {
        itemData.image = req.file.filename;
    }

    try {
        const image = new Item(itemData);

        await image.save();
        return res.send({message: 'Добавлено!', image: image});
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const item = await Item.findById(req.params.id).populate('user');

        if (item) {
            return res.send(item);
        } else {
            return res.status(404).send({message: 'Ненайдено!'});
        }

    } catch {
        return res.status(404).send({message: 'Ненайдено!'});
    }
});

router.delete('/:id', auth, async (req, res) => {
    try {
        await Item.deleteOne({_id: req.params.id});
        return res.sendStatus(200).send({message: 'Удалено!'});
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.put('/:id', auth, async (req, res) => {
    let item = await Item.findById(req.params.id);

        item.rating.push({
            item: req.body.item,
            rating: req.body.rating,
            review: req.body.review,
            datetime: new Date().toISOString(),
            anonymous: req.body.anonymous,
            displayName: req.body.displayName,
        });
        await item.save()
            .then(result => res.send({result, message: "Рейтинг выставлен!"}))
            .catch(error => res.status(400).send("Оценить могут только зарегистрированые пользоватили!"));
});

module.exports = router;