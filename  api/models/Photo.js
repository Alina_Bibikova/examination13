
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PhotoSchema = new Schema({
     photo: {
        type: String,
        required: 'Картинка объязательна!'
    },
    items: {
        type: Schema.Types.ObjectId,
        ref: 'Items',
        required: 'Объязательное поле!'
    },
});

const Photo = mongoose.model('Photo', PhotoSchema);

module.exports = Photo;