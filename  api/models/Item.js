const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ItemSchema = new Schema({
    title: {
        type: String,
        required: 'Введите название!'
    },
    description: {
        type: String,
    },
    image: {
        type: String,
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: 'Пользователь объязательный!'
    },
    rating: [{
        item: String, rating: Number, review: String, datetime: String, anonymous: Boolean, displayName: String,
    }],
    question: {
        type: String,
    },

});

const Item = mongoose.model('Item', ItemSchema);

module.exports = Item;