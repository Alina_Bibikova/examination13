const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CommentSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    item: {
        type: Schema.Types.ObjectId,
        ref: 'Items',
        required: true
    },
    description: {
        type: String,
        required: true
    },
});

const Comment = mongoose.model('Comment', CommentSchema);

module.exports = Comment;