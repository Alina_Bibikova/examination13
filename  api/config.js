const path = require('path');

const rootPath = __dirname;

const dbUrl = process.env.NODE_ENV === 'test' ? 'mongodb://localhost/exam13_test': 'mongodb://localhost/exam13';
const wsURL = process.env.NODE_ENV === 'start' ? 'http://localhost:8010': 'http://localhost:8000';

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    dbUrl,
    wsURL,
    mongoOptions: {
        useNewUrlParser: true,
        useCreateIndex: true
    },
};