const mongoose = require('mongoose');
const nanoid = require('nanoid');
const config = require('./config');

const User = require('./models/User');
const Item = require('./models/Item');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const users = await User.create({
                username: 'user',
                password: '123',
                displayName: 'Василий Петров',
                role: 'user',
                token: nanoid()

            },

            {
                username: 'admin',
                password: '123',
                displayName: 'Александр Марк',
                role: 'admin',
                token: nanoid()
            });

    await Item.create(
        {
            title: 'Мамина дача',
            description: 'Описание заведения',
            image: 'image2.jpeg',
            user: users[0]._id
        },
        {
            title: 'Папина дача',
            description: 'Описание заведения',
            image: 'image4.jpg',
            user: users[0]._id
        },
        {
            title: 'Сына дача',
            description: 'Описание заведения',
            image: 'image3.jpg',
            user: users[1]._id
        },
    );

        await connection.close();
    };

    run().catch(error => {
        console.error('Something went wrong', error);
    });